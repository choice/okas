#!/usr/bin/perl -w
# $Id$


# Truth hurts -glen, 2003-03-31
die "Running this script as root is EXTREMELY BAD IDEA\n" unless $<;



use strict;
use Sys::Hostname;
use IO::Pipe;
use POSIX;
use File::Temp qw/tempdir/;
use delfi::mycnf;
use delfi::db;

my $debug = 1 if -t STDOUT;
my ($host, $c, $tmpdir, $date);
my ($ssh_remote, $remote_dir);

my @zip_args = qw(zip -X0mjrq);
my @p7zip_args = qw(7z a -mx=0 -bd -tzip);

my @copy_args = qw(--quiet);
my @db_list;

my $conf = "$ENV{HOME}/.backup.conf";
if (@ARGV) {
	$conf = shift;
}
$c = new delfi::mycnf($conf);

$tmpdir = $c->get('options', 'tmpdir') || $ENV{TMPDIR} || '/tmp';
$tmpdir = tempdir( DIR => $tmpdir);

my @include = $c->get('databases', 'include');
my @exclude = $c->get('databases', 'exclude');

if ($c->get('options', 'noindices')) {
	push(@copy_args, '--noindices');
}

my $use_p7zip = $c->get('options', 'use_p7zip');
my $outdir = $c->get('options', 'outdir') || '/opt/backup/mysql';

$date = strftime("%Y%m%d", localtime());

($host) = (hostname =~ m#^(\w+)#);


END {
	# check for $tmpdir, since END block is even called we fail to 'use' some perl library.
	if ($tmpdir) {
		printf "Deleting %s/*\n", $tmpdir if $debug;
		foreach my $dir (<$tmpdir/*>) {
			printf " delete %s", $dir;
			unlink <$dir/*>;
			rmdir $dir;
			printf "\n";
		}
		rmdir $tmpdir;
	}
}

sub pipe_remote {
	my $db = shift;

	my $file = "$host-$db-$date.zip";

	# we can't create zip file and pipe it to remote, as this requires output
	# file seekable, but it might not be possible to seek in output, therefore
	# we create local file first. this also decreases time remote connection is up.

	my @shell = ('scp', "$outdir/$file", "$ssh_remote:$remote_dir/$file");
	printf STDERR " %s\n", join(', ', @shell) if $debug;
	system(@shell) == 0 or die "scp failed: $?";
}

sub archive_local {
	my $db = shift;

	mkdir $outdir unless -d $outdir;

	my $file = "$outdir/$host-$db-$date.zip";

	# zip tends to update archive. we don't want that.
	unlink($file);

	# to get relative paths, we need to chdir for p7zip
	chdir("$tmpdir/$db") or die $!;

	my @shell = ($use_p7zip ? @p7zip_args : @zip_args, $file, '.');
	system(@shell) == 0 or die "zip failed: $?";
}


sub hotcopy {
	my $db = shift;

	printf " hotcopy: " if $debug;

	# database to backup
	my $arg = $db;

	# add table exclusions from this db
	my @excl = $c->get($db, 'exclude');
	$arg .= sprintf('./~^`?(%s)/', join('|', @excl)) if @excl;

	my @shell = ('mysqlhotcopy', @copy_args, $arg, $tmpdir);
	if ($c->get($db, 'noindices')) {
		push(@shell, '--noindices');
	}

	printf STDERR " %s\n", join(', ', @shell) if $debug;
	system(@shell) == 0
		or die "mysqlhotcopy failed: $?\n";

	printf "done\n" if $debug;
}


sub compress_tables {
	my $db = shift;

	my $compress = 0;

	unless ($c->get('options', 'compress_tables') || $c->get($db, 'compress_tables')) {
		return;
	}

	printf " compress tables: " if $debug;

	my $fact = $c->get('options', 'compress_level') || 9;
	my @shell = ('bzip2', "-$fact", <$tmpdir/$db/*>);
#	printf STDERR " %s\n", join(', ', @shell) if $debug;
	system(@shell) == 0
		or die "bzip2 failed: $?\n";

	printf "done\n" if $debug;
}




if (@exclude or !@include) {
	my $dbh = new delfi::db($c->get('client', 'section') || 'client');
	$dbh->login;
	my $sth = $dbh->prepare("show databases");
	$sth->execute;
	while (my($name) = $sth->fetchrow_array) {
		next if $name eq 'information_schema';
		push @db_list, $name unless grep { m{^\Q$name\E$} } @exclude;
	}
	$sth->finish;
	$dbh->logout;
}

# setup params for hotcopy
{
	# get mysql connection info
	my $dbconf = $c;
	my $section = $dbconf->get('client', 'section');
	# if we have section defined, change config to ~/.my.cnf
	if ($section) {
		$dbconf = new delfi::mycnf();
	} else {
		$section = 'client';
	}

	my $user = $dbconf->get($section, 'user');
	my $socket = $dbconf->get($section, 'socket');
	push(@copy_args, ('-u', $user)) if $user;
	push(@copy_args, ('-S', $socket)) if $socket;
}


*ORIGOUT = *ORIGOUT; # prevent warning
open(ORIGOUT, ">&STDOUT") or die "Can't dup STDOUT:  $!";

push @db_list, @include;

$ssh_remote = $c->get('options', 'sshhost');
$remote_dir = $c->get('options', 'netdir') || 'net/mysql';

# make it unique
my %k = map {$_ => 1} @db_list;
printf "%d databases to backup\n", scalar keys(%k) if $debug;
foreach my $db (keys(%k)) {
	printf "%s...\n", $db if $debug;
	hotcopy($db);
	compress_tables($db);

	printf " archive local: " if $debug;
	archive_local($db);
	printf "done\n" if $debug;

	if ($tmpdir) {
		printf " deleting $db/* " if $debug;
		unlink <$tmpdir/$db/*>;
		rmdir "$tmpdir/$db";
		printf "done\n" if $debug;
	}

	if ($ssh_remote) {
		printf " pipe remote: " if $debug;
		pipe_remote($db);
		printf "done\n" if $debug;
	}
}
