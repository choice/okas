#!/bin/sh
set -e

dir=$(dirname "$0")
cd $dir

m=`date -d '1 month ago' +%Y%m`
dirs=`ls -d */$m 2>/dev/null`

[ "$dirs" ] || exit

dst="`hostname -s`-apache-logs"
mkdir -p "$dst"
for dir in $dirs; do
	echo -n "$dir"
	pd=`dirname "$dir"`
	mkdir -p "$dst/$pd"
	mv "$dir" "$dst/$pd"
	echo ""
done

find "$dst" -type l | xargs -r rm -v
