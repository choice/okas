#!/bin/sh
# Builds rpm from the files in CVS
# Author: glen@delfi.ee

set -e

# check build deps
rc=0
for pkg in rpm-specdump perl-tools-pod fakeroot alien dpkg debhelper; do
	o=$(rpm -q $pkg) || { rc=$?; echo >&2 "$o"; }
done
test "$rc" -gt 0 && exit $rc

# build rpm
_topdir="${PWD:-$(pwd)}"
dist_dir=$_topdir/dist

rm -rf $dist_dir
install -d $dist_dir

# update timestamps from last commit
# http://stackoverflow.com/questions/1964470/whats-the-equivalent-of-use-commit-times-for-git/5531813#5531813
update_timestamps() {
	local dir="$1"
	set +x
	echo "Updating timestamps from last commit, please wait..."
	git ls-files | while read file; do
		rev=$(git rev-list -n 1 HEAD "$file")
		file_time=$(git show --pretty=format:%ai --abbrev-commit $rev | head -n 1)
		touch -d "$file_time" "$dir/$file"
	done
}
# WARNING: we're changing original files, this may confuse git if this is your real git repo
update_timestamps $_topdir

unset TMPDIR || :

# add changelog similarily to pld builder script
# https://github.com/pld-linux/rpm-build-tools/blob/b34dcd91d2ca8fdbd70a38f39ded002f80d20cca/builder.sh#L460
specfile=$(mktemp)
gitlog=$(mktemp)
cat etc/okas.spec > $specfile
# rpm5.org/rpm.org do not parse any other date format than 'Wed Jan 1 1997'
# otherwise i'd use --date=iso here
# http://rpm5.org/cvs/fileview?f=rpm/build/parseChangelog.c&v=2.44.2.1
# http://rpm.org/gitweb?p=rpm.git;a=blob;f=build/parseChangelog.c;h=56ba69daa41d65ec9fd18c9f371b8ff14118cdca;hb=a113baa510a004476edc44b5ebaaf559238a18b6#l33
# NOTE: changelog date is always in UTC for rpmbuild
# * 1265749244 +0000 Random Hacker <nikt@pld-linux.org> 9370900
git rev-list --date-order -${log_entries:-20} HEAD 2>/dev/null | while read sha1; do
	local logfmt='%B%n'
	git notes list $sha1 > /dev/null 2>&1 && logfmt='%N'
	git log -n 1 $sha1 --format=format:"* %ad %an <%ae> %h%n- ${logfmt}%n" --date=raw | sed -re 's/^- +- */- /'| sed '/^$/q'
done > $gitlog

echo '%changelog' >> $specfile
LC_ALL=C gawk '/^\* /{printf("* %s %s\n", strftime("%a %b %d %Y", $2), substr($0, length($1)+length($2)+length($3)+4)); next}{print}' $gitlog >> $specfile

rpmbuild \
	--define "_topdir $_topdir" \
	--define '_builddir %{_topdir}' \
	--define "_rpmdir $dist_dir" \
	--define '_sourcedir %{_topdir}' \
	--define '_specdir %{_topdir}' \
	--define '_buildchangelogtruncate %{nil}' \
	--define 'buildroot %{_topdir}/buildroot' \
	--define 'skip_prep 1' \
	--define "rel $BUILD_NUMBER" \
	--define 'clean %%clean \
	exit 0%{nil}' \
	--define '__spec_clean_body %{nil}' \
	-bb $specfile

rm $specfile

rpm=$(ls $dist_dir/*.rpm 2>/dev/null)
if [ ! -f "$rpm" ]; then
	echo >&2 "RPM not built!"
	exit 1
fi

# gnupg signing
key=$(rpm --eval '%{_gpg_name}')
if [ "$key" ] && [ "$key" != '%{_gpg_name}' ]; then
	echo "Signing package with gpg key $key"
	rpm --addsign "$rpm"
fi

# build debian
if [ ! -x /usr/bin/fakeroot ]; then
	echo >&2 "!! fakeroot not found; debian pkg will be skipped"
	exit 0
fi

V=$(rpm -qp --qf '%{V}' $rpm)
rm -rf okas-$V
alien --scripts -d -g -s -k $rpm
cp -p okas-$V/debian/changelog debian
cp -p okas-$V/debian/copyright debian
fakeroot debian/rules binary
