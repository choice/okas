#!/bin/sh
# preuninstall script for okas
# $Id$
. /usr/lib/okas/functions
umask 002

/usr/lib/okas/uninst.sh XTerm
/usr/lib/okas/uninst.sh bashrc
/usr/lib/okas/uninst.sh vim
/usr/lib/okas/uninst.sh yum
/usr/lib/okas/uninst.sh poldek
/usr/lib/okas/uninst.sh subversion
