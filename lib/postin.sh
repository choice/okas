#!/bin/sh
# postinstall script for okas
# $Id$
umask 002

/usr/lib/okas/inst.sh XTerm
/usr/lib/okas/inst.sh bashrc
/usr/lib/okas/inst.sh rpm.macros
/usr/lib/okas/inst.sh vim
/usr/lib/okas/inst.sh sysctl.conf
/usr/lib/okas/inst.sh yum
/usr/lib/okas/inst.sh poldek
/usr/lib/okas/inst.sh apt
/usr/lib/okas/inst.sh subversion
