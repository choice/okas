Summary:	glen's (and others) vision of nice linux setup
Name:		okas
Version:	1.6
Release:	%{rel}
License:	Ask glen@alkohol.ee
Group:		Applications/System
URL:		http://glen.alkohol.ee/okas/
BuildRequires:	/usr/bin/pod2man
Requires(post,preun,triggerin):	grep
Requires(post,preun,triggerin):	sed
Conflicts:	keychain < 2.5.0
Conflicts:	poldek < 0.21-0.20070703.00.15.15
Conflicts:	poldek-delfi < 1.3
BuildArch:	noarch
BuildRoot:	%{tmpdir}/%{name}-%{version}-root-%(id -u -n)

%define		_datadir	%{_prefix}/share/%{name}
%define		_libdir		%{_prefix}/lib/%{name}

%define		tmpdir %(echo "${TMPDIR:-/tmp}")
%define		_unpackaged_files_terminate_build    0

# We can't let RPM do the dependencies automatic because it'll then pick up
# a correct but undesirable perl dependency from the utility scripts, which
# aren't really needed on all systems
AutoReqProv:	no

# force gz payload for broken rh7.3 hybrid
%define		_binary_payload	w9.gzdio

%description
This package adds some nice scripts, configures your bashrc and vim.
Also adds some XTerm .Xdefaults

This package is signed, you can import the key via:
rpm --import %{_datadir}/okas.asc

THESE PROGRAMS COME WITH NO WARRANTY! I'M NOT RESPONSIBLE OF ANY DATA
LOSS CAUSED BY USING THESE SCRIPTS.

%clean
rm -rf $RPM_BUILD_ROOT

%prep
%setup -qTD -n .

%build
%{__make}

%install
rm -rf $RPM_BUILD_ROOT
%{__make} install \
	DESTDIR=$RPM_BUILD_ROOT

%post
%{_libdir}/postin.sh

%preun
if [ "$1" = 0 ]; then
	%{_libdir}/preun.sh
fi

%triggerin -- vim-common, vim-rt
%{_libdir}/inst.sh vim

%triggerin -- XFree86
%{_libdir}/inst.sh XFree86

%triggerpostun -- %{name} < 1.5-1.506
%{_libdir}/inst.sh yum
%{_libdir}/inst.sh poldek

%files
%defattr(644,root,root,755)
%config(noreplace) %verify(not md5 mtime size) /etc/bashrc.local

%attr(755,root,root) %{_bindir}/*
%attr(755,root,root) %{_sbindir}/*

%dir %{_datadir}
%{_datadir}/XTerm.ad
%config %{_datadir}/bashrc
%config %{_datadir}/ssh-auth-sock
%{_datadir}/cdhist.bash
%{_datadir}/fg.bash
%{_datadir}/df.awk
%{_datadir}/okas.asc
%{_datadir}/yum.repo
%{_datadir}/poldek.repo
%{_datadir}/apt.repo
%{_datadir}/subversion
%config %{_datadir}/DIR_COLORS
%config %{_datadir}/glen.kmap
%config %{_datadir}/glen.map
%config %{_datadir}/screenrc
%config %{_datadir}/sysctl.conf

%dir %{_datadir}/vim
%config %{_datadir}/vim/okas.vim
%config %{_datadir}/vim/vimrc

%dir %{_datadir}/ssh-agent
%{_datadir}/ssh-agent/bash*

%dir %{_libdir}
%{_libdir}/functions
%attr(755,root,root) %{_libdir}/inst.sh
%attr(755,root,root) %{_libdir}/postin.sh
%attr(755,root,root) %{_libdir}/preun.sh
%attr(755,root,root) %{_libdir}/uninst.sh

%{_mandir}/man1/*.1*
%{_mandir}/man5/okas.5*
