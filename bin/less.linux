#!/bin/sh
# Attempts to save contents of the screen of linux console
# before displaying manpage, and restore after the display.
# Expects variable $TTY having value of tty(1) before execution.
#
# NOTE: This program is not secure. It does not check what is in TTYCTL
# do not use this program in enviroment you don't trust.
#
# Author: <glen@delfi.ee>
# $Id$
#
# ChangeLog:
# 2000-10-29 - can use less args also, not only stdin
#            - rename to less.linux. don't check for tty, use $TTYCTL variable.
#              define that in profile
# 2000-10-28 - restore colours before screen; use trap for cleanup
# 2000-10-17 - use /dev/vcsa not screendump, to catch attributes
# 2001-03-22 - fix quote problem
# 2001-05-24 - don't restore screen contents if less returned error
# 2001-06-24 - make the TTYCTL variable in this script. don't depend on bashrc

temp=$(mktemp ${TMPDIR-/tmp}/lessXXXXXX)
trap "rm -f '$temp'; exit 1" 1 2 3 15

TTYCTL=
# we check from stderr if $TTY is empty , as stdin/stdout are already redirected
tty=${TTY:-$(tty < /dev/stderr)}
ttyno=${tty#/dev/tty}
ttyno=${ttyno#/dev/vc/}

# are we on real console?
if [ "$tty" = "/dev/console" ]; then
	if [ -w /dev/vcc/a -a -r /dev/vcc/a ]; then
		TTYCTL="/dev/vcc/a"
	elif [ -w /dev/vcsa -a -r /dev/vcsa ]; then
		TTYCTL="/dev/vcsa"
	fi
else
	case "$ttyno" in
	# are we on virtual console (/dev/ttyN+) ?
	[0-9]*)
		if [ -w /dev/vcc/a$ttyno -a -r /dev/vcc/a$ttyno ]; then
			TTYCTL=/dev/vcc/a$ttyno
		elif [ -w /dev/vcsa$ttyno -a -r /dev/vcsa$ttyno ]; then
			TTYCTL=/dev/vcsa$ttyno
		fi
	;;
	esac
fi

[ "$TTYCTL" ] && cat "$TTYCTL" > "$temp"
echo -n "\033]PF00CA00\033[1;11]"

/usr/bin/less -is "$@"; rc=$?

# restore
echo -n "\033]R\033[8]"
if [ "$rc" = 0 -a "$TTYCTL" ]; then
	cat "$temp" > "$TTYCTL"
fi
rm -f "$temp"
